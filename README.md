# µD3TN Docker Images

## About

This repository provides docker images for [µD3TN](https://gitlab.com/d3tn/ud3tn).
See https://gitlab.com/d3tn/ud3tn-docker-images/container_registry for a list of available tags.

## Usage

- `docker run registry.gitlab.com/d3tn/ud3tn-docker-images/ud3tn [µD3TN command line options]`
- `docker run registry.gitlab.com/d3tn/ud3tn-docker-images/ud3tn --help`

**Note:** µD3TN uses UNIX domain sockets for AAP and AAP 2.0 communication by default. Specify `--aap-host 0.0.0.0` (for AAP) or `--aap2-host 0.0.0.0` (for AAP 2.0) to use TCP instead. Optionally, specify a port of your choice via `--aap-port PORT` (for AAP) or `--aap2-port PORT` (for AAP 2.0). Make sure to publish all the ports you are interested in via the `docker run` options `--publish , -p` or `--publish-all , -P`.
